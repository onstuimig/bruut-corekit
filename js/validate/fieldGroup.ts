class FieldGroup extends FieldBase {

	fieldGroup;
	fieldGroupName;
	target;

	constructor(field, Form) {
		super(field, Form);

		this.fieldGroup = false;
		this.fieldGroupName = false;

		this.getFieldValidationProperties();
		this.setupGroup();
		this.addGroupRules();
		this.target = null;

		this.setupValidationByEvent();
	}

	addGroupRules() {
		this.config.rules['max-checked'] = (data, that) => {
			var max = data;

			let totalChecked = 0;

			for (var i = 0; i < that.fieldGroup.length; i++) {
				if(that.fieldGroup[i].checked)
					totalChecked++
			}

			if (totalChecked > max) {
				return false;
			}

			return true;
		}

		this.config.rules['min-checked'] = (data, that) => {
			var min = data;

			let totalChecked = 0;

			for (var i = 0; i < that.fieldGroup.length; i++) {
				if(that.fieldGroup[i].checked)
					totalChecked++
			}

			if (totalChecked < min) {
				return false;
			}

			return true;
		}

		this.config.rules['match-password'] = (data, that) => {

			if (!that.target) {
				let match = true;
				let firstVal = '';
				for(let i = 0, len = that.fieldGroup.length; i < len; ++i) {
					if (i==0) firstVal = that.fieldGroup[i].value.trim();

 					if(that.fieldGroup[i].value.trim() !== firstVal) {
  						match = false;
 					}

				}

				if (firstVal == '') return false;

				return match;
			} else {
				var password1 = that.target.val();
				var password2 = that.fieldGroup.not(that.target).first().val();

				if (password1 == '') return false;
				if (password1 === password2) {
					return true;
				}

				return false;
			}

		}
	}

	setupGroup() {
		this.fieldGroup = this.FormInstance.form.querySelectorAll(`[${this.config.attributes.fieldGroupChild}="${this.fieldGroupName}"]`);
	}

	validate(rulesByEvent) {

		for (var rule in this.fieldConfig) {
			if (this.fieldConfig.hasOwnProperty(rule)) {
				// Rule + ruleValue
				//console.log(rule + " -> " + this.fieldConfig[rule]);

			if (rule in this.config.rules) {
				/*
					check the rules if:
					rulesByEvent is undefined = check all fieldrules
					rulesByEvent = check rules given by event
				*/

				if (typeof rulesByEvent == 'undefined' || (rulesByEvent.indexOf(rule) >= 0)) {
						if (typeof rulesByEvent == 'undefined' || (rulesByEvent.indexOf(rule) >= 0)) {
							var tmpError = this.config.rules[rule](this.fieldConfig[rule], this);
					if (tmpError != true) {
						this.fieldErrors[`${this.fieldGroupName}__${rule}`] = tmpError;
						this.FormInstance.fieldErrors[`${this.fieldGroupName}__${rule}`] = tmpError;
					} else {
						delete this.fieldErrors[`${this.fieldGroupName}__${rule}`];
						delete this.FormInstance.fieldErrors[`${this.fieldGroupName}__${rule}`];
					}
				}
			}
				}
			}
		}

		this.checkFieldErrors();
	}

	setupValidationByEvent() {
		for (var eventName in this.config.rulesOnEvents) {
			if (this.config.rulesOnEvents.hasOwnProperty(eventName)) {
				(eventName == 'keyup') ? this.setupEventsKeyup(eventName, this.config.rulesOnEvents[eventName]) : this.setupEvents(eventName, this.config.rulesOnEvents[eventName]);
	}
		}
	}

	setupEvents(eventName, rulesByEvent) {
		var rulesByEvent = rulesByEvent;
		var eventName = eventName;

		for (var i = 0; i < this.fieldGroup.length; i++) {
			this.fieldGroup[i].addEventListener(eventName, (ev) => {
				this.target = ev.currentTarget;
			this.validate(rulesByEvent);
		});
	}
	}

	setupEventsKeyup(eventName, rulesByEvent) {
		var rulesByEvent = rulesByEvent;

		this.field.addEventListener(eventName,
			debounce((ev) => {
				this.target = ev.target;
				this.validate(rulesByEvent);
			}, this.config.validationDelay)
		);
	}

	/**
	 * Get the field validation properties from the 'validate' attribute
	 * @return this.collectFieldChecks() checks which checks are in the Config and adds them to the Watcher Object.
	 */
	getFieldValidationProperties() {
		var props = this.field.getAttribute(this.config.attributes.fieldGroup).replace(/ /g, '').split(',');

		for (var i = 0; i < props.length; i++) {
			var tmprule = props[i].split(':');
			this.fieldConfig[tmprule[0]] = tmprule[1] || '';
		}

		if ('group' in this.fieldConfig) {
			this.fieldGroupName = this.fieldConfig['group'];

			delete this.fieldConfig['group'];

			// Push a data attribute to the element with the name of the group in it, to
			// retrieve the error messages correctly.
			this.field.setAttribute(this.config.attributes.fieldName, this.fieldGroupName);

			this.isGroupField = true;
			this.getParentNodes();
		}
	}
}
