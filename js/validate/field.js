var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Field = (function (_super) {
    __extends(Field, _super);
    function Field(field, Form) {
        var _this = _super.call(this, field, Form) || this;
        _this.fieldCombines = false;
        if (!_this.field.hasAttribute(_this.fieldNameAttribute))
            _this.fieldNameAttribute = 'name';
        _this.setupGlueGroup();
        _this.getFieldValidationProperties();
        _this.setupValidationByEvent();
        return _this;
    }
    Field.prototype.validate = function (rulesByEvent) {
        var _this = this;
        jQuery.each(this.fieldConfig, function (rule, ruleValue) {
            if (rule in _this.config.rules) {
                if (typeof rulesByEvent == 'undefined' || (rulesByEvent.indexOf(rule) >= 0)) {
                    var tmpError = _this.config.rules[rule](ruleValue, _this.field.value);
                    if (tmpError != true) {
                        _this.fieldErrors[_this.$field.attr(_this.fieldNameAttribute) + "__" + rule] = tmpError;
                        _this.FormInstance.fieldErrors[_this.$field.attr(_this.fieldNameAttribute) + "__" + rule] = tmpError;
                        _this.FormInstance.fieldsWithErrors[_this.$field.attr(_this.fieldNameAttribute)] = true;
                    }
                    else {
                        delete _this.fieldErrors[_this.$field.attr(_this.fieldNameAttribute) + "__" + rule];
                        delete _this.FormInstance.fieldErrors[_this.$field.attr(_this.fieldNameAttribute) + "__" + rule];
                        if (_this.FormInstance.fieldsWithErrors[_this.$field.attr(_this.fieldNameAttribute)] == 'undefined') {
                            delete _this.FormInstance.fieldsWithErrors[_this.$field.attr(_this.fieldNameAttribute)];
                        }
                    }
                }
            }
        });
        this.checkFieldErrors();
    };
    Field.prototype.setupValidationByEvent = function () {
        var _this = this;
        jQuery.each(this.config.rulesOnEvents, function (eventName, rulesByEvent) {
            if (eventName == 'keyup') {
                _this.setupEventsKeyup(eventName, rulesByEvent);
            }
            else {
                _this.setupEvents(eventName, rulesByEvent);
            }
        });
    };
    Field.prototype.setupEvents = function (eventName, rulesByEvent) {
        var _this = this;
        if (this.fieldCombines.length > 0) {
            this.fieldCombines.on(eventName, function (e) {
                _this.setValueByGlue();
                _this.validate(rulesByEvent);
            });
            jQuery(this.field).on(eventName, function (e) {
                _this.setGlueFieldValuesByField();
                _this.validate(rulesByEvent);
            });
        }
        else {
            jQuery(this.field).on(eventName, function (e) {
                _this.validate(rulesByEvent);
            });
        }
    };
    Field.prototype.setupEventsKeyup = function (eventName, rulesByEvent) {
        var _this = this;
        var rulesByEvent = rulesByEvent;
        if (this.fieldCombines.length > 0) {
            this.fieldCombines.on(eventName, debounce(function () {
                _this.setValueByGlue();
                _this.validate(rulesByEvent);
            }, this.config.validationDelay));
            jQuery(this.field).on(eventName, debounce(function () {
                _this.setGlueFieldValuesByField();
                _this.validate(rulesByEvent);
            }, this.config.validationDelay));
        }
        else {
            jQuery(this.field).on(eventName, debounce(function () {
                _this.validate(rulesByEvent);
            }, this.config.validationDelay));
        }
    };
    Field.prototype.getFieldValidationProperties = function () {
        var _this = this;
        var props = this.field.getAttribute(this.config.attributes.fieldValidationSettings).replace(/ /g, '').split(',');
        jQuery(props).each(function (i, v) {
            var tmprule = v.split(':');
            _this.fieldConfig[tmprule[0]] = tmprule[1] || null;
        });
    };
    Field.prototype.setupGlueGroup = function () {
        if (this.field.hasAttribute(this.config.attributes.fieldCombineGlue)) {
            this.fieldParent = this.config.field.parentGroupClass;
            this.fieldCombines = this.formObject.find("[" + this.config.attributes.fieldCombine + "=\"" + this.field.getAttribute(this.config.attributes.fieldCombineGlue) + "\"]");
            this.fieldNameAttribute = this.config.attributes.fieldCombine;
            this.isCombineField = true;
            this.fieldNameAttribute = this.config.attributes.fieldCombineGlue;
        }
        this.getParentNodes();
    };
    Field.prototype.setValueByGlue = function () {
        var glueValues = [];
        this.fieldCombines.each(function (i, element) {
            glueValues[i] = element.value;
        });
        this.field.value = glueValues.join(this.field.getAttribute(this.config.attributes.fieldGlueCharacter)).trim();
    };
    Field.prototype.setGlueFieldValuesByField = function () {
        var glueValues = this.field.value.split(this.field.getAttribute(this.config.attributes.fieldGlueCharacter), this.fieldCombines.length);
        this.fieldCombines.each(function (i, element) {
            element.value = (typeof (glueValues[i]) != 'undefined') ? glueValues[i] : '';
        });
    };
    return Field;
}(FieldBase));
