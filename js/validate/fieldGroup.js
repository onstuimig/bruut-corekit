var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var FieldGroup = (function (_super) {
    __extends(FieldGroup, _super);
    function FieldGroup(field, Form) {
        var _this = _super.call(this, field, Form) || this;
        _this.fieldGroup = false;
        _this.fieldGroupName = false;
        _this.getFieldValidationProperties();
        _this.setupGroup();
        _this.addGroupRules();
        _this.target = null;
        _this.setupValidationByEvent();
        return _this;
    }
    FieldGroup.prototype.addGroupRules = function () {
        this.config.rules['max-checked'] = function (data, that) {
            var max = data;
            var totalChecked = that.fieldGroup.filter(':checked').length;
            if (totalChecked > max) {
                that.target.prop('checked', false);
                that.fieldGroup.trigger("maxCheckedError", [that.target, max, totalChecked]);
                return true;
            }
            return true;
        };
        this.config.rules['min-checked'] = function (data, that) {
            var min = data;
            var totalChecked = that.fieldGroup.filter(':checked').length;
            if (totalChecked < min) {
                return false;
            }
            return true;
        };
        this.config.rules['match-password'] = function (data, that) {
            if (!that.target) {
                var match = true;
                var firstVal = '';
                for (var i = 0, len = that.fieldGroup.length; i < len; ++i) {
                    if (i == 0)
                        firstVal = that.fieldGroup[i].value.trim();
                    if (that.fieldGroup[i].value.trim() !== firstVal) {
                        match = false;
                    }
                }
                if (firstVal == '')
                    return false;
                return match;
            }
            else {
                var password1 = that.target.val();
                var password2 = that.fieldGroup.not(that.target).first().val();
                if (password1 == '')
                    return false;
                if (password1 === password2) {
                    return true;
                }
                return false;
            }
        };
    };
    FieldGroup.prototype.setupGroup = function () {
        this.fieldGroup = this.formObject.find("[" + this.config.attributes.fieldGroupChild + "=\"" + this.fieldGroupName + "\"]");
    };
    FieldGroup.prototype.validate = function (rulesByEvent) {
        var _this = this;
        jQuery.each(this.fieldConfig, function (rule, ruleValue) {
            if (rule in _this.config.rules) {
                if (typeof rulesByEvent == 'undefined' || (rulesByEvent.indexOf(rule) >= 0)) {
                    var tmpError = _this.config.rules[rule](ruleValue, _this);
                    if (tmpError != true) {
                        _this.fieldErrors[_this.fieldGroupName + "__" + rule] = tmpError;
                        _this.FormInstance.fieldErrors[_this.fieldGroupName + "__" + rule] = tmpError;
                    }
                    else {
                        delete _this.fieldErrors[_this.fieldGroupName + "__" + rule];
                        delete _this.FormInstance.fieldErrors[_this.fieldGroupName + "__" + rule];
                    }
                }
            }
        });
        this.checkFieldErrors();
    };
    FieldGroup.prototype.setupValidationByEvent = function () {
        var _this = this;
        jQuery.each(this.config.rulesOnEvents, function (eventName, rulesByEvent) {
            (eventName == 'keyup') ? _this.setupEventsKeyup(eventName, rulesByEvent) : _this.setupEvents(eventName, rulesByEvent);
        });
    };
    FieldGroup.prototype.setupEvents = function (eventName, rulesByEvent) {
        var _this = this;
        var rulesByEvent = rulesByEvent;
        var eventName = eventName;
        jQuery(this.fieldGroup).on(eventName, function (ev) {
            _this.target = jQuery(ev.currentTarget);
            _this.validate(rulesByEvent);
        });
    };
    FieldGroup.prototype.setupEventsKeyup = function (eventName, rulesByEvent) {
        var _this = this;
        var rulesByEvent = rulesByEvent;
        jQuery(this.field).on(eventName, debounce(function (ev) {
            _this.target = jQuery(ev.target);
            _this.validate(rulesByEvent);
        }, this.config.validationDelay));
    };
    FieldGroup.prototype.getFieldValidationProperties = function () {
        var _this = this;
        var props = this.field.getAttribute(this.config.attributes.fieldGroup).replace(/ /g, '').split(',');
        jQuery(props).each(function (i, v) {
            var tmprule = v.split(':');
            _this.fieldConfig[tmprule[0]] = tmprule[1] || '';
        });
        if ('group' in this.fieldConfig) {
            this.fieldGroupName = this.fieldConfig['group'];
            delete this.fieldConfig['group'];
            this.$field.attr(this.config.attributes.fieldName, this.fieldGroupName);
            this.isGroupField = true;
            this.getParentNodes();
        }
    };
    return FieldGroup;
}(FieldBase));
