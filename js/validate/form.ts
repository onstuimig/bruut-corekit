class FormValidate {
	public form;
	field;
	formFields;
	formGroups;
	fieldErrors;
	isValid;
	fieldsWithErrors;

	config = {
		validationDelay: 		150,

		classes: {
			error: 				'has-errors',
			success: 			'is-ok',
			touched: 			'is-touched'
		},

		rules: {

			'required': (length, fieldValue) => {
				return (fieldValue.trim() == '' ? false : true);
			},

			'min-length': (length, fieldValue) => {
				return (fieldValue.length >= length);
			},

			'max-length': (length, fieldValue) => {
				return (fieldValue.length <= length);
			},

			'characters': (type, fieldValue) => {

				if (type === 'alpha') {
					var pattern = /^[a-zA-Z]*$/;
					return (pattern.test(fieldValue))
				} else if (type === 'alphaspace') {
					var pattern = /^[a-zA-Z\s]*$/;
					return (pattern.test(fieldValue))
				} else if (type === 'alphaspacedash') {
					var pattern = /^[a-zA-Z\s-]*$/;
					return (pattern.test(fieldValue))
				} else if (type === 'numeric') {
					if (fieldValue.length == 0)
						return true;

					var mixed_var = fieldValue;

					/* source: phpjs.org is_numeric() */
					var whitespace =
						' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000'
					return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) ===
						-1)) && mixed_var !== '' && !isNaN(mixed_var)

				} else {
					console.warn('characters property on field can only contain "alpha|numeric|alphaspace|alphaspacedash"');
				}
			},

			'email': (data, fieldValue) => {
				if (fieldValue.length == 0)
					return true;

				var pattern = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;

				return (pattern.test(fieldValue));
			},

			'postcode': (data, fieldValue) => {
				if (fieldValue.length == 0)
					return true;

				var pattern = /^[1-9][0-9]{3}[\s]?(?!SS|SA|SD)[A-Z]{2}$/i;

				return !!fieldValue.match(pattern);
			},

			'date': (data, fieldValue) => {
				if (fieldValue.length == 0)
					return true;
				/* dutch date check + schrikkel jaar */

				var pattern = /^(?:(?:31(\/|-|\.|\s)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.|\s)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.|\s)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.|\s)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
				return !!fieldValue.match(pattern);
			}
		},

		rulesOnEvents: {
			'keyup': ['min-length', 'max-length', 'email', 'characters', 'required', 'postcode', 'date', 'match-password'],
			'change': ['min-checked', 'max-checked', 'match-password']
		},

		field: {
			parentClass: 				'.form-element',
			parentGroupClass: 			'.form-group',
		},

		attributePrefix: 'v',

		attributes: {
			// Attribute that matches the field.name inside this wrapper
			fieldName:  				'name',

			// Attribute for defining the properties and validation options
			fieldValidationSettings: 	'props',

			// Attribute for defining a field group
			fieldGroup: 				'group',
			fieldGroupChild:   			'group-child',

			// Attribute for defining a character to connect the fieldCombine inputs too
			fieldCombine: 				'combine',
			fieldCombineGlue: 			'glue-result',
			fieldGlueCharacter: 		'glue-character',

			// Attribute for defining the validation message for the field wrapper
			fieldMessage:  				'message-for',

			// Attribute that defines the html for the error messages for the field wrapper
			fieldMessageErrorHtml: 		'error-for',

			// Attribute that defines the html for the success message for the field wrapper
			fieldMessageOkHtml: 		'ok-for',
		}
	}

	constructor(form, userConfiguration?: any) {

		this.form 				= (form.nodeType) ? form : form[0];
		this.formFields			= [];
		this.formGroups			= [];
		this.fieldErrors 		= {};
		this.isValid			= false;
		this.fieldsWithErrors 	= [];

		this.config = Object.deepExtend(this.config, userConfiguration);

		for (var key in this.config.attributes) {
			if (this.config.attributes.hasOwnProperty(key)) {
				this.config.attributes[key] = `${this.config.attributePrefix}-${this.config.attributes[key]}`;
			}
		}

		this.update();
		this.handleSubmit();
	}

	update() {
		// Fetch all fields
		var _fields 		= this.form.querySelectorAll(`[${this.config.attributes.fieldValidationSettings}]`);

		// Fetch all field groups
		var _fieldGroups 	= this.form.querySelectorAll(`[${this.config.attributes.fieldGroup}]`);

		/**
		 * Create a new Field Class for all form fields in this form
		 */
		for (var i = 0; i < _fields.length; i++) {
 			this.formFields[i] = new Field(_fields[i], this);
 		}

		for (var i = 0; i < _fieldGroups.length; i++) {
 			this.formGroups[i] = new FieldGroup(_fieldGroups[i], this);
 		}
	}

	handleSubmit() {

		this.form.addEventListener('submit', (e) => {

			e.preventDefault();
			this.validate();

			return (this.isValid) ? this.form.submit() : false;
		});
	}

	addValidator(obj) {
		this.config.rules[obj['validatorName']] = obj.logic;
		this.config.rulesOnEvents[ obj['onEvent']].push( obj['validatorName'] );
	}

	validate(callback?: any) {

		/**
		 * Run trough each formField and formGroup and call the validate() method
		 */
		Array.prototype.forEach.call(this.formFields, (el, i) => {
			this.formFields[i].validate();
		});

		Array.prototype.forEach.call(this.formGroups, (el, i) => {
			this.formGroups[i].validate();
		});

		this.isValid = (Object.keys(this.fieldErrors).length == 0 ) ? true : false;

		if(typeof callback == 'function') {
			callback();
		}

		// TODO: make this vanilla jQuery(this).trigger('validated', this.isValid);

		return this.isValid;
	}
}

Object.deepExtend = function(destination, source) {
  for (var property in source) {
    if (source[property] && source[property].constructor &&
     source[property].constructor === Object) {
      destination[property] = destination[property] || {};
      arguments.callee(destination[property], source[property]);
    } else {
      destination[property] = source[property];
    }
  }
  return destination;
};

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};

/**
 * Get all DOM element up the tree that contain a class, ID, or data attribute
 * @param  {Node} elem The base element
 * @param  {String} selector The class, id, data attribute, or tag to look for
 * @return {Array} Null if no match
 */
var getParents = function (elem, selector) {

    var parents = [];
    var firstChar;
    if ( selector ) {
        firstChar = selector.charAt(0);
    }

    // Get matches
    for ( ; elem && elem !== document; elem = elem.parentNode ) {
        if ( selector ) {

            // If selector is a class
            if ( firstChar === '.' ) {
                if ( elem.classList.contains( selector.substr(1) ) ) {
                    parents.push( elem );
                }
            }

            // If selector is an ID
            if ( firstChar === '#' ) {
                if ( elem.id === selector.substr(1) ) {
                    parents.push( elem );
                }
            }

            // If selector is a data attribute
            if ( firstChar === '[' ) {
                if ( elem.hasAttribute( selector.substr(1, selector.length - 1) ) ) {
                    parents.push( elem );
                }
            }

            // If selector is a tag
            if ( elem.tagName.toLowerCase() === selector ) {
                parents.push( elem );
            }

        } else {
            parents.push( elem );
        }

    }

    // Return parents if any exist
    if ( parents.length === 0 ) {
        return null;
    } else {
        return parents;
    }

};
