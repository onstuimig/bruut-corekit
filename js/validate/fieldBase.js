var FieldBase = (function () {
    function FieldBase(field, Form) {
        this.field = field;
        this.fieldConfig = {};
        this.fieldErrors = {};
        this.FormInstance = Form;
        this.config = Form.config;
        this.fieldParent = this.config.field.parentClass;
        this.fieldNameAttribute = this.config.attributes.fieldName;
        this.isGroupField = false;
        this.isCombineField = false;
        this.parents = false;
    }
    FieldBase.prototype.getParentNodes = function () {
        if (this.isGroupField) {
            this.parents = this.field;
        }
        else if (this.isCombineField) {
            this.parents = this.FormInstance.form.querySelectorAll("[" + this.config.attributes.fieldCombine + "=\"" + this.field.getAttribute(this.fieldNameAttribute) + "\"]")[0];
            this.parents = getParents(this.parents, "" + this.config.field.parentGroupClass)[0];
        }
        else {
            this.parents = this.FormInstance.form.querySelectorAll("[" + this.fieldNameAttribute + "=\"" + this.field.getAttribute(this.fieldNameAttribute) + "\"]")[0];
            this.parents = getParents(this.parents, "" + this.config.field.parentClass)[0];
        }
    };
    FieldBase.prototype.checkFieldErrors = function () {
        (Object.keys(this.fieldErrors).length == 0) ? this.fieldIsOk() : this.fieldHasErrors();
        this.fieldIsTouched();
    };
    FieldBase.prototype.fieldIsTouched = function () {
        this.parents.classList.add(this.config.classes.touched);
    };
    FieldBase.prototype.fieldHasErrors = function () {
        this.parents.classList.add(this.config.classes.error);
        this.parents.classList.remove(this.config.classes.success);
        var messageBoxes = this.FormInstance.form.querySelectorAll("[" + this.config.attributes.fieldMessage + "=\"" + this.field.getAttribute(this.fieldNameAttribute) + "\"]");
        for (var i = 0; i < messageBoxes.length; i++) {
            messageBoxes[i].classList.remove(this.config.classes.success);
            messageBoxes[i].classList.add(this.config.classes.error);
            messageBoxes[i].innerHTML = this.FormInstance.form.querySelector("[" + this.config.attributes.fieldMessageErrorHtml + "=\"" + this.field.getAttribute(this.fieldNameAttribute) + "\"]").innerHTML;
        }
    };
    FieldBase.prototype.fieldIsOk = function () {
        this.parents.classList.remove(this.config.classes.error);
        this.parents.classList.add(this.config.classes.success);
        var messageBoxes = this.FormInstance.form.querySelectorAll("[" + this.config.attributes.fieldMessage + "=\"" + this.field.getAttribute(this.fieldNameAttribute) + "\"]");
        for (var i = 0; i < messageBoxes.length; i++) {
            messageBoxes[i].classList.remove(this.config.classes.error);
            messageBoxes[i].classList.add(this.config.classes.success);
            messageBoxes[i].innerHTML = this.FormInstance.form.querySelector("[" + this.config.attributes.fieldMessageOkHtml + "=\"" + this.field.getAttribute(this.fieldNameAttribute) + "\"]").innerHTML;
        }
    };
    return FieldBase;
}());
