class Field extends FieldBase {

	fieldCombines;

	constructor(field, Form) {
		super(field, Form);

		this.fieldCombines = false;

		/* fallback to field name if v-name attibute does not exist */
		if(!this.field.hasAttribute(this.fieldNameAttribute))
			this.fieldNameAttribute = 'name';

		this.setupGlueGroup();
		this.getFieldValidationProperties();
		this.setupValidationByEvent();
	}

	validate(rulesByEvent) {

		for (var rule in this.fieldConfig) {
			if (this.fieldConfig.hasOwnProperty(rule)) {
				// Rule + ruleValue
				//console.log(rule + " -> " + this.fieldConfig[rule]);

				if ( rule in this.config.rules) {
					/*
						check the rules if:
						rulesByEvent is undefined = check all fieldrules
						rulesByEvent = check rules given by event
					*/

					if (typeof rulesByEvent == 'undefined' || (rulesByEvent.indexOf(rule) >= 0)) {

						var tmpError = this.config.rules[rule](this.fieldConfig[rule], this.field.value);

						if (tmpError != true) {
							this.fieldErrors[`${this.field.getAttribute(this.fieldNameAttribute)}__${rule}`] = tmpError;
							this.FormInstance.fieldErrors[`${this.field.getAttribute(this.fieldNameAttribute)}__${rule}`] = tmpError;

							this.FormInstance.fieldsWithErrors[ this.field.getAttribute(this.fieldNameAttribute) ] = true;
						} else {
							delete this.fieldErrors[`${this.field.getAttribute(this.fieldNameAttribute)}__${rule}`];
							delete this.FormInstance.fieldErrors[`${this.field.getAttribute(this.fieldNameAttribute)}__${rule}`];

							if(this.FormInstance.fieldsWithErrors[ this.field.getAttribute(this.fieldNameAttribute) ] == 'undefined' ) {
								delete this.FormInstance.fieldsWithErrors[ this.field.getAttribute(this.fieldNameAttribute) ];
							}
						}
					}
				}
			}
		}

		this.checkFieldErrors();
	}

	setupValidationByEvent() {
		for (var eventName in this.config.rulesOnEvents) {
			if (this.config.rulesOnEvents.hasOwnProperty(eventName)) {
				(eventName == 'keyup') ? this.setupEventsKeyup(eventName, this.config.rulesOnEvents[eventName]) : this.setupEvents(eventName, this.config.rulesOnEvents[eventName]);
			}
		}
	}

	setupEvents(eventName, rulesByEvent) {
		if (this.fieldCombines.length > 0) {

			for (var i = 0; i < this.fieldCombines.length; i++) {
				this.fieldCombines[i].addEventListener(eventName, () => {
					this.setValueByGlue();
					this.validate(rulesByEvent);
				});
			};

			this.field.addEventListener(eventName, (e) => {
				this.setGlueFieldValuesByField();
				this.validate(rulesByEvent);
			});

		} else {
			this.field.addEventListener(eventName, (e) => {
				this.validate(rulesByEvent);
			});
		}
	}

	setupEventsKeyup(eventName, rulesByEvent) {
		var rulesByEvent = rulesByEvent;

		if (this.fieldCombines.length > 0) {

			for (var i = 0; i < this.fieldCombines.length; i++) {
				this.fieldCombines[i].addEventListener(eventName,
					debounce(() => {
						this.setValueByGlue();
						this.validate(rulesByEvent);
					}, this.config.validationDelay)
				);
			}

			this.field.addEventListener(eventName,
				debounce(() => {
					this.setGlueFieldValuesByField();
					this.validate(rulesByEvent);
				}, this.config.validationDelay)
			);
		} else {
			this.field.addEventListener(eventName,
				debounce(() => {
					this.validate(rulesByEvent);
				}, this.config.validationDelay)
			);
		}
	}

	/**
	 * Get the field validation properties from the 'validate' attribute
	 * @return this.collectFieldChecks() checks which checks are in the Config and adds them to the Watcher Object.
	 */
	getFieldValidationProperties() {
		var props = this.field.getAttribute(this.config.attributes.fieldValidationSettings).replace(/ /g, '').split(',');

		for (var i = 0; i < props.length; i++) {
			var tmprule = props[i].split(':');
			this.fieldConfig[tmprule[0]] = tmprule[1] || null;
		}
	}

	setupGlueGroup() {
		if (this.field.hasAttribute(this.config.attributes.fieldCombineGlue)) {

			this.fieldParent		= this.config.field.parentGroupClass;

			this.fieldCombines = this.FormInstance.form.querySelectorAll(`[${this.config.attributes.fieldCombine}="${this.field.getAttribute(this.config.attributes.fieldCombineGlue)}"]`);

			this.fieldNameAttribute = this.config.attributes.fieldCombine;

			this.isCombineField 	= true;
			this.fieldNameAttribute = this.config.attributes.fieldCombineGlue;
		}

		this.getParentNodes();
	}

	/* glue glueField value from combine fields */
	setValueByGlue() {
		var glueValues = [];

		for (var i = 0; i < this.fieldCombines.length; i++) {
		    glueValues[i] = this.fieldCombines[i].value;
		}

		this.field.value = glueValues.join(this.field.getAttribute( this.config.attributes.fieldGlueCharacter )).trim();
	}

	/* separate glueField to combine fields */
	setGlueFieldValuesByField() {
		var glueValues = this.field.value.split(this.field.getAttribute(this.config.attributes.fieldGlueCharacter), this.fieldCombines.length);

		for (var i = 0; i < this.fieldCombines.length; i++) {
		    this.fieldCombines[i].value = ( typeof(glueValues[i]) != 'undefined' ) ? glueValues[i] : '';
		}
	}
}
