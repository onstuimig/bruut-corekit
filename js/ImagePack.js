var LazyResponsiveImage = (function () {
    function LazyResponsiveImage(object, userConfiguration) {
        this._typeBackground = 'background';
        this._typeImage = 'image';
        this.config = {
            loadedClass: 'is-loaded',
            type: 'image',
            noLazyClass: 'no-lazy',
            baseDataAttribute: 'data-img-src',
            viewportOffset: 250,
            imageSizes: {
                481: 'mobile',
                768: 'small',
                1024: 'tablet'
            }
        };
        var extend = function () {
            var extended = {};
            for (key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        this.config = extend(this.config, userConfiguration);
        this.imageContainer = object;
        this.isLazyLoadNeeded();
    }
    LazyResponsiveImage.prototype.isLazyLoadNeeded = function () {
        if (this.imageContainer.classList.contains(this.config.noLazyClass)) {
            this.collectImageInfo();
        }
        else {
            this.createScrollMonitor();
        }
    };
    LazyResponsiveImage.prototype.createScrollMonitor = function () {
        this.imageContainerWatcher = scrollMonitor.create(this.imageContainer, this.config.viewportOffset);
        this.checkEnterViewport();
    };
    LazyResponsiveImage.prototype.checkEnterViewport = function () {
        var _this = this;
        this.imageContainerWatcher.enterViewport(function () {
            if (!_this.imageContainer.classList.contains(_this.config.loadedClass)) {
                _this.collectImageInfo();
            }
            else {
                _this.destroyScrollMonitor();
            }
        });
    };
    LazyResponsiveImage.prototype.collectImageInfo = function () {
        if (this.config.type === this._typeImage) {
            this.image = this.imageContainer.querySelectorAll('img')[0];
            this.imageSource = this.image.getAttribute(this.getImageSource(this.image));
            if (this.imageSource) {
                this.image.src = this.imageSource;
                this.handleImageLoad(this.image);
            }
        }
        else if (this.config.type === this._typeBackground) {
            this.imageSource = this.imageContainer.getAttribute(this.getImageSource(this.imageContainer));
            if (this.imageSource) {
                var tempImage = new Image();
                tempImage.src = this.imageSource;
                this.handleImageLoad(tempImage);
            }
        }
        else {
            console.warn('No allowed type (Allowed types: [' + this._typeImage + ' or ' + this._typeBackground + ']) found in passed configuration object. Aborting...');
            this.destroyScrollMonitor();
        }
    };
    LazyResponsiveImage.prototype.handleImageLoad = function (image) {
        var _this = this;
        if (this.config.type === this._typeBackground) {
            image.onload = function () {
                _this.setBackgroundImage();
            };
        }
        else {
            image.onload = function () {
                _this.imageContainer.classList.add(_this.config.loadedClass);
                _this.destroyScrollMonitor();
            };
        }
        image.onerror = function () {
            _this.imageContainer.classList.add('error-404');
            _this.destroyScrollMonitor();
        };
    };
    LazyResponsiveImage.prototype.setBackgroundImage = function () {
        this.imageContainer.style.backgroundImage = "url('" + this.imageSource + "')";
        this.imageContainer.classList.add(this.config.loadedClass);
        this.destroyScrollMonitor();
    };
    LazyResponsiveImage.prototype.destroyScrollMonitor = function () {
        if (typeof this.imageContainerWatcher != 'undefined') {
            this.imageContainerWatcher.destroy();
        }
    };
    LazyResponsiveImage.prototype.getImageSource = function (element) {
        this.screenWidth = this.getWindowWidth();
        var responsiveDataPostfix = '';
        for (var key in this.config.imageSizes) {
            if (this.config.imageSizes.hasOwnProperty(key)) {
                if (this.screenWidth < parseInt(key) && element.hasAttribute(this.config.baseDataAttribute + "-" + this.config.imageSizes[key])) {
                    responsiveDataPostfix = "-" + this.config.imageSizes[key];
                    break;
                }
            }
        }
        return this.config.baseDataAttribute + responsiveDataPostfix;
    };
    LazyResponsiveImage.prototype.getWindowWidth = function () {
        return window.innerWidth;
    };
    return LazyResponsiveImage;
}());
